# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from decimal import Decimal
from io import BytesIO
from trytond.model import fields
from trytond.pool import Pool, PoolMeta

# import barcode
# from barcode.writer import ImageWriter
ImageWriter = None

try:
    import barcode
    from barcode.writer import ImageWriter
except:
    pass


writer_options = {
    'module_width': 0.2,
    'module_height': 6.0,
    'quiet_zone': 1,
    'font_size': 10,
    'text_distance': 0.2,
    'background': 'white',
    'foreground': 'black',
    'write_text': False,
    'text': None,
}

CODE_PRICE = {
    '1': 'N',
    '2': 'i',
    '3': 'D',
    '4': 'Y',
    '5': 'A',
    '6': 'M',
    '7': 'R',
    '8': 'T',
    '9': 'Z',
    '0': 'C',
}


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    img_barcode = fields.Function(fields.Binary('Image Barcode'),
        'get_img_barcode')
    encoded_cost_price = fields.Function(fields.Char('Encoded Cost Price'),
        'get_encoded_price')
    encoded_sale_price = fields.Function(fields.Char('Encoded Sale Price'),
        'get_encoded_price')

    def get_encoded_price(self, name=None, value=None):
        string = ''
        name = name[8:] if name else None
        if value:
            string = str(int(value))
        elif name == 'cost_price':
            if self.template.cost_price:
                string = str(int(self.get_final_cost_w_tax()))
        elif name == 'sale_price':
            if self.template.sale_price_w_tax:
                string = str(int(self.template.sale_price_w_tax))

        value = string
        for v in string:
            value = value.replace(v, CODE_PRICE[v])
        return value

    def get_final_cost_w_tax(self, name=None):
        Tax = Pool().get('account.tax')
        Template = Pool().get('product.template')
        res = None
        supplier_taxes = []
        cost_price = self.last_cost or self.template.cost_price
        if self.supplier_taxes_used and cost_price:
            for tax in self.supplier_taxes_used:
                if tax.type == 'percentage' and tax.rate > 0:
                    supplier_taxes.append(tax)
            if supplier_taxes:
                tax_list = Tax.compute(supplier_taxes,
                    cost_price or Decimal('0.0'), 1)
                res = sum([t['amount'] for t in tax_list], Decimal('0.0'))
                res = res + cost_price
                res = res.quantize(
                    Decimal(1) / 10 ** Template.cost_price.digits[1])
        if not res:
            return cost_price
        return res

    def get_img_barcode(self, name=None):
        value = None
        if ImageWriter and self.code:
            output = BytesIO()
            barcode_img = barcode.get('CODE39', self.code, writer=ImageWriter())
            img_barcode = barcode_img.render(writer_options, self.code)

            img_barcode.save(output, format='PNG')
            value = output.getvalue()
            output.close()
        return value
