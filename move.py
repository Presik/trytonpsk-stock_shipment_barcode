# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta
# from trytond.transaction import Transaction
# from datetime import date


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'
    number_tags = fields.Integer('Tags')

    @classmethod
    def create(cls, vlist):
        moves = super(Move, cls).create(vlist)
        for move in moves:
            move.number_tags = int(move.quantity)
            move.save()
        return moves
